#pragma once


#include <algorithm.hpp>

#include <cstring>


namespace rs
{
        /** Stacks are a type of container adaptor, specifically designed to operate
         *  in a LIFO context (last-in first-out), where elements are inserted and
         *  extracted only from one end of the container.
         */
        template <class Type>
        class stack
        {
        private: // Fields.

                Type* begin;
                Type* end;

        public: // Methods.

                /** Default constructor.
                 */
                stack() noexcept
                : begin(nullptr)
                , end(nullptr)
                {
                }

                /** Copy constructor.
                 */
                stack(stack<Type>& other)
                : begin(new Type[other.size()])
                , end(begin + other.size())
                {
                        memcpy(begin, other.begin, other.size());
                        *(end - 1) = *(other.end - 1); // For some reason without this line the last element is lost.
                }

                /** Move constructor.
                 */
                stack(stack<Type>&& other) noexcept
                : begin(other.begin)
                , end(other.end)
                {
                        other.begin = nullptr;
                        other.end = nullptr;
                }

                /** Destructor.
                 */
                ~stack()
                {
                        if (!empty()) delete[] begin;
                }

                size_t size() const noexcept
                {
                        return end - begin;
                }

                void push(Type& value)
                {
                        put(value);
                }

                void push(Type&& value)
                {
                        put(value);
                }

                template <class... Args>
                void emplace(Args&... args)
                {
                        put(Type(args...));
                }

                void pop()
                {
                        pop_front();
                }

                Type& top()
                {
                        return front();
                }

                void swap(stack<Type>& other) noexcept
                {
                        std::swap(this->begin, other.begin);
                        std::swap(this->end, other.end);
                }

        public: // Range methods.

                bool empty() const
                {
                        return !begin || !end || begin >= end;
                }

                stack<Type> save()
                {
                        return stack<Type>(*this);
                }

                void pop_front()
                {
                        assert(!empty());
                        *--end = 0;
                }

                Type& front()
                {
                        assert(!empty());
                        return *(end - 1);
                }

                void put(Type& value)
                {
                        if (empty())
                        {
                                begin = new Type[1];
                                end = begin;
                        }
                        else
                        {
                                const size_t curr_size = size();
                                const Type* temp = begin;

                                begin = new Type[curr_size + 1];
                                memcpy(begin, temp, sizeof(Type) * curr_size);
                                delete[] temp;
                                
                                end = begin + curr_size;
                        }
                        *end++ = value;
                }

                void put(Type&& value)
                {
                        put(value); // This is now passing an lvalue reference so the method above is called.
                }
        };
}

template <class Type>
bool operator ==(rs::stack<Type>& lhs, rs::stack<Type>& rhs)
{
        return rs::equal(lhs, rhs);
}

template <class Type>
bool operator ==(rs::stack<Type>&& lhs, rs::stack<Type>&& rhs)
{
        return lhs == rhs;
}