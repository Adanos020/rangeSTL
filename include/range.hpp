#pragma once


#include <cassert>
#include <cstddef>


/** The following ranges must provide the following methods.
 *
 *  Input range:
 *  - empty
 *  - pop front
 *  - front
 *
 *  Forward range:
 *  - all from input range
 *  - save
 *
 *  Bidirectional range:
 *  - all from forward range
 *  - pop back
 *  - back
 *
 *  Random access range (finite):
 *  - all from bidirectional range
 *  - operator []
 *  - at
 *  - size
 *
 *  Random access range (infinite):
 *  - all from forward range
 *  - operator []
 *  - at
 *
 *  Output range:
 *  - put (both for lvalue and rvalue references)
 */
namespace rs
{
        // Utility classes.

        /** Contiguous range of values.
         * 
         *  This is a forward range range.
         */
        template <class Type>
        class contig_range
        {
        private:

                Type* begin;
                Type* end;

        public:

                contig_range(Type* begin, Type* end)
                : begin(begin)
                , end(end)
                {
                }

                bool empty() const
                {
                        return begin >= end;
                }

                void pop_front()
                {
                        assert(!empty());
                        ++begin;
                }

                Type& front()
                {
                        assert(!empty());
                        return *begin;
                }
        };

        /** Contiguous range of values.
         * 
         *  This is a bidirectional range.
         */
        template <class Type>
        class bicontig_range
        {
        private:

                Type* begin;
                Type* end;

        public:

                bicontig_range()
                : begin(nullptr)
                , end(nullptr)
                {
                }

                bicontig_range(Type* begin, Type* end)
                : begin(begin)
                , end(end)
                {
                }

                bicontig_range(bicontig_range<Type>& other)
                : begin(other.begin)
                , end(other.end)
                {
                }

                bicontig_range(bicontig_range<Type>&& other)
                : bicontig_range(other)
                {
                }

                bool empty() const
                {
                        return begin >= end;
                }

                bicontig_range<Type> save()
                {
                        return bicontig_range(*this);
                }

                void pop_front()
                {
                        assert(!empty());
                        ++begin;
                }

                void pop_back()
                {
                        assert(!empty());
                        --end;
                }

                Type& front()
                {
                        assert(!empty());
                        return *begin;
                }

                Type& back()
                {
                        assert(!empty());
                        return *(end - 1);
                }
        };

        /** Reversed version of any bidirectional range.
         * 
         *  This is a bidirectional range.
         */
        template <template <class T> class BidirectionalRange, class Type>
        class reversed_range
        {
        public:

                BidirectionalRange<Type> range;

        public:

                reversed_range(BidirectionalRange<Type>& range)
                : range(range)
                {
                }

                reversed_range(reversed_range<BidirectionalRange, Type>& other)
                : range(BidirectionalRange<Type>(other.range))
                {
                }

                bool empty() const
                {
                        return range.empty();
                }

                reversed_range<BidirectionalRange, Type> save()
                {
                        return reversed_range(*this);
                }

                void pop_front()
                {
                        range.pop_back();
                }

                void pop_back()
                {
                        range.pop_front();
                }

                Type& front()
                {
                        return range.back();
                }

                Type& back()
                {
                        return range.front();
                }
        };

        // Utility functions.

        /**
         */
        template <template<class T> class BidirectionalRange, class Type>
        inline reversed_range<BidirectionalRange, Type> retro(BidirectionalRange<Type>& range)
        {
                return reversed_range(range);
        }

        /**
         */
        template <template<class T> class BidirectionalRange, class Type>
        inline reversed_range<BidirectionalRange, Type> retro(BidirectionalRange<Type>&& range)
        {
                return retro(range);
        }

        /**
         */
        template <template<class T> class BidirectionalRange, class Type>
        inline BidirectionalRange<Type> retro(reversed_range<BidirectionalRange, Type>& range)
        {
                return BidirectionalRange(range.range);
        }

        /**
         */
        template <template<class T> class BidirectionalRange, class Type>
        inline BidirectionalRange<Type> retro(reversed_range<BidirectionalRange, Type>&& range)
        {
                return retro(range);
        }
}