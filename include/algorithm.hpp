#pragma once


#include <range.hpp>

#include <utility>


namespace rs
{
        // Non-modifying sequence operations:

        /** \fn all_of
         *  Checks whether all elements of given range match given condition.
         * 
         *  \param range        Input range whose elements are checked.
         *  \param pred         Function returning a boolean value based on one parameter,
         *                      containing the condition for sought elements.
         * 
         *  \return `true` if all elements match the condition, `false` otherwise.
         */
        template <class InputRange, class UnaryPredicate>
        inline bool all_of(InputRange range, UnaryPredicate pred)
        {
                for (; !range.empty(); range.pop_front())
                {
                        if (!pred(range.front())) return false;
                }
                return true;
        }

        /** \fn any_of
         *  Checks whether at least one element of given range matches given condition.
         * 
         *  \param range        Input range whose elements are checked.
         *  \param pred         Function returning a boolean value based on one parameter,
         *                      containing the condition for sought elements.
         * 
         *  \return `true` if any element matches the condition, `false` otherwise.
         */
        template <class InputRange, class UnaryPredicate>
        inline bool any_of(InputRange range, UnaryPredicate pred)
        {
                for (; !range.empty(); range.pop_front())
                {
                        if (pred(range.front())) return true;
                }
                return false;
        }

        /** \fn none_of
         *  Checks whether no elements of given range match given condition.
         * 
         *  \param range        Input range whose elements are checked.
         *  \param pred         Function returning a boolean value based on one parameter,
         *                      containing the condition for sought elements.
         * 
         *  \return `false` if any element matches the condition, `true` otherwise.
         */
        template <class InputRange, class UnaryPredicate>
        inline bool none_of(InputRange range, UnaryPredicate pred)
        {
                return !any_of(range, pred);
        }

        /** \fn for_each
         *  Performs an operation on all elements of given range.
         * 
         *  \param range        Forward range on whose elements the operation is performed.
         *  \param func         Function performing some operation on an element.
         * 
         *  \return Pointer to the given function.
         */
        template <class ForwardRange, class Function>
        inline Function for_each(ForwardRange range, Function func)
        {
                for (; !range.empty(); range.pop_front())
                {
                        func(range.front());
                }
                return std::move(func);
        }

        /** \fn find_if
         *  Finds the first occurrence of a value matching given condition in given range.
         * 
         *  \param range        Input range in which the value is searched for.
         *  \param pred         Function returning a boolean value based on one parameter,
         *                      containing the condition for an element sought.
         * 
         *  \return Subrange of the input range starting with the first occurrence
         *          of value sought. If the value is not found, empty range is 
         *          returned.
         */
        template <class InputRange, class UnaryPredicate>
        inline InputRange find_if(InputRange range, UnaryPredicate pred)
        {
                for (; !range.empty(); range.pop_front())
                {
                        if (pred(range.front())) break;
                }
                return range;
        }

        /** \fn find
         *  Finds the first occurrence of given value in given range.
         * 
         *  \param range        Input range in which the value is searched for.
         *  \param value        Value that is searched for.
         * 
         *  \return Subrange of the input range starting with the first occurrence
         *          of given value. If the value is not found, empty range is returned.
         */
        template <class InputRange, class Type>
        inline InputRange find(InputRange range, const Type& value)
        {
                return find_if(range, [&](auto x){ return x == value; });
        }

        /** \fn find_end
         *  Finds the last occurrence of a given value in given range.
         * 
         *  \param range        Bidirectional range in which the value is searched for.
         *  \param value        Value that is searched for.
         * 
         *  \return Subrange of the bidirectional range ending with the last occurrence
         *          of given value. If the value is not found, empty range is returned.
         */
        template <class BidirectionalRange, class Type>
        inline BidirectionalRange find_end(BidirectionalRange range, const Type& value)
        {
                return retro(find(retro(range), value));
        }

        /** \fn find_first_of
         *  Finds the first occurrence of a value in the first range matching given condition
         *  with the next value of the second range.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         *  \param pred         Function returning a boolean value based on two parameters,
         *                      containing the condition for sought elements.
         * 
         *  \return Subrange of the first range starting with the first occurrence of a value
         *          matching the condition with the next value in the second range.
         */
        template <class InputRange1, class InputRange2, class BinaryPredicate>
        inline InputRange1 find_first_of(InputRange1 range1, InputRange2 range2, BinaryPredicate pred)
        {
                for (; !range2.empty(); range2.pop_front())
                {
                        InputRange1 found = find_if(range1, range2.front(),
                                [&](auto x){ return pred(x, range2.front()); });
                        if (!found.empty()) break;
                }
                return range1;
        }

        /** \fn find_first_of
         *  Finds the first occurrence of a value from the second range in the first range.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         * 
         *  \return Subrange of the first range starting with the first occurrence of a value
         *          found in the second range.
         */
        template <class InputRange1, class InputRange2>
        inline InputRange1 find_first_of(InputRange1 range1, InputRange2 range2)
        {
                return find_first_of(range1, range2, [](auto x, auto y){ return x == y; });
        }

        /** \fn adjacent_find
         *  Finds the first occurrence of two equal adjacent values in given range.
         * 
         *  \param range        Forward range where the values are looked for.
         * 
         *  \return Subrange of given range starting with the two equal adjacent values.
         *          If no such values are found, an empty range is returned.
         */
        template <class ForwardRange>
        inline ForwardRange adjacent_find(ForwardRange range)
        {
                if (!range.empty())
                {
                        ForwardRange temp = range.save();
                        temp.pop_front();
                        for (; !temp.empty(); range.pop_front(), temp.pop_front())
                        {
                                if (temp.front() == range.front()) break;
                        }
                }
                return range;
        }

        /** \fn count
         *  Counts the occurrences of values matching given condition in given range.
         * 
         *  \param range        Input range where the occurrences are counted.
         *  \param pred         Function returning a boolean value based on one parameter,
         *                      containing the condition for an element sought.
         * 
         *  \return Number of occurrences of given value. 
         */
        template <class InputRange, class UnaryPredicate>
        inline size_t count_if(InputRange range, UnaryPredicate pred)
        {
                size_t counter = 0;

                for_each(range, [&](auto& x)
                {
                        if (pred(x)) ++counter;
                });

                return counter;
        }

        /** \fn count
         *  Counts the occurrences of a value in given range.
         * 
         *  \param range        Input range where the occurrences are counted.
         *  \param value        Value whose occurrences in the range are counted.
         * 
         *  \return Number of occurrences of given value.
         */
        template <class InputRange, class Type>
        inline size_t count(InputRange range, Type value)
        {
                return count_if(range, [&](auto x){ return x == value; });
        }

        /** \fn mismatch
         *  Finds the positions at which first two given ranges' values match given condition.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         *  \param pred         Function returning a boolean value based on two parameters,
         *                      containing the condition for sought elements.
         * 
         *  \return Pair of subranges starting with first elements matching given condition.
         */
        template <class InputRange1, class InputRange2, class BinaryPredicate>
        inline std::pair<InputRange1, InputRange2> mismatch(InputRange1 range1, InputRange2 range2, BinaryPredicate pred)
        {
                for (; !range1.empty() && !range2.empty(); range1.pop_front(), range2.pop_front())
                {
                        if (pred(range1.front(), range2.front())) break;
                }
                return std::make_pair(range1, range2);
        }

        /** \fn mismatch
         *  Finds the positions at which first two given ranges' values are unequal.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         * 
         *  \return Pair of subranges starting with first mismatching elements.
         */
        template <class InputRange1, class InputRange2>
        inline std::pair<InputRange1, InputRange2> mismatch(InputRange1 range1, InputRange2 range2)
        {
                return mismatch(range1, range2, [](auto x, auto y){ return x != y; });
        }

        /** \fn equal
         *  Checks whether all elements of two given ranges match given condition.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         *  \param pred         Function returning a boolean value based on two parameters,
         *                      containing the condition for sought elements.
         * 
         *  \return `true` if all elements of both ranges match the condition, `false` otherwise.  
         */
        template <class InputRange1, class InputRange2, class BinaryPredicate>
        inline bool equal(InputRange1 range1, InputRange2 range2, BinaryPredicate pred)
        {
                for (; !range1.empty() && !range2.empty(); range1.pop_front(), range2.pop_front())
                {
                        if (!pred(range1.front(), range2.front())) return false;
                }
                return range1.empty() && range2.empty();
        }

        /** \fn equal
         *  Checks whether all elements of two given ranges are equivalent.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         * 
         *  \return `true` if both ranges are equal, `false` otherwise.  
         */
        template <class InputRange1, class InputRange2>
        inline bool equal(InputRange1 range1, InputRange2 range2)
        {
                return equal(range1, range2, [](auto x, auto y){ return x == y; });
        }

        /** \fn is_permutation
         *  Checks whether two ranges contain the same values, disregarding the order.
         * 
         *  \param range1       First input range.
         *  \param range2       Second input range.
         * 
         *  \return `true` if one range is a permutation of the other, `false` otherwise.
         */
        template <class ForwardRange, class InputRange>
        inline bool is_permutation(ForwardRange range1, InputRange range2)
        {
                for (ForwardRange temp = range1.save(); !temp.empty(); temp.pop_front())
                {
                        auto current = temp.front();
                        if (count(range1, current) != count(range2, current)) return false;
                }
                return true;
        }

        /** \fn search
         *  Finds given sequence in given range.
         * 
         *  \param range        Forward range where the sequence is sought.
         *  \param sequence     Forward range defining the sequence sought.
         * 
         *  \return Subrange of given range starting at the position where the sought sequence
         *          occurs. If the sequence is not found, an empty range is returned.
         */
        template <class ForwardRange1, class ForwardRange2, class BinaryPredicate>
        inline ForwardRange1 search(ForwardRange1 range, ForwardRange2 sequence, BinaryPredicate pred)
        {
                if (sequence.empty()) return range;

                for (; !range.empty(); range.pop_front())
                {
                        for (auto range_cp = range.save(), sequence_cp = sequence.save();
                             pred(range_cp.front(), sequence_cp.front());
                             range_cp.pop_front(), sequence_cp.pop_front())
                        {
                                if (sequence_cp.empty()) return range;
                                if (range_cp.empty())    return range_cp;
                        }
                }
                return range;
        }

        /** \fn search
         *  Finds the first occurrence of given sequence in given range.
         * 
         *  \param range        Forward range where the sequence is sought.
         *  \param sequence     Forward range defining the sought sequence.
         * 
         *  \return Subrange of given range starting at the position where the sought sequence
         *          occurs. If the sequence is not found, an empty range is returned.
         */
        template <class ForwardRange1, class ForwardRange2>
        inline ForwardRange1 search(ForwardRange1 range, ForwardRange2 sequence)
        {
                return search(range, sequence, [](auto x, auto y){ return x == y; });
        }

        /** \fn search_n
         *  Finds the first occurrence of sequence of `n` values all matching given condition
         *  with given value in given range.
         * 
         *  \param range        Forward range in which the sequence is sought.
         *  \param n            Number of elements in the sought sequence.
         *  \param value        Value filling the sequence.
         *  \param pred         Function returning a boolean value based on two parameters,
         *                      containing the condition for sought elements.
         * 
         *  \return Subrange of given range starting at the position where the sought sequence
         *          occurs. If the sequence is not found, an empty range is returned.
         */
        template <class ForwardRange, class Size, class Type, class BinaryPredicate>
        inline ForwardRange search_n(ForwardRange range, Size n, const Type& value, BinaryPredicate pred)
        {
                for (; !range.empty(); range.pop_front())
                {
                        Size counter = 0;
                        for (ForwardRange temp = range.save(); pred(temp.front(), value); temp.pop_front())
                        {
                                if (++counter == n) return range;
                                if (temp.empty())   return temp;
                        }
                }
                return range;
        }

        /** \fn search_n
         *  Finds the first occurrence of sequence of `n` values all equal to given value
         *  in given range.
         * 
         *  \param range        Forward range in which the sequence is sought.
         *  \param n            Number of elements in the sought sequence.
         *  \param value        Value filling the sequence.
         * 
         *  \return Subrange of given range starting at the position where the sought sequence
         *          occurs. If the sequence is not found, an empty range is returned.
         */
        template <class ForwardRange, class Size, class Type>
        inline ForwardRange search_n(ForwardRange range, Size n, const Type& value)
        {
                return search_n(range, n, value, [](auto x, auto y){ return x == y; });
        }


        // Modifying sequence operations:

        /** \fn copy
         *  Copies the elements from one range and inserts them into the other. Warning: unlike
         *  std::copy it doesn't replace any values in the destination range.
         * 
         *  \param source       Input range from which values are copied.
         *  \param destination  Output range to which copied values are inserted.
         * 
         *  \return Resultant destination range.
         */
        template <class InputRange, class OutputRange>
        inline OutputRange& copy(InputRange source, OutputRange& destination)
        {
                for_each(source, [&](auto x)
                {
                        destination.put(x);
                });
                return destination;
        }

        /** \fn copy_n
         *  Copies the elements from one range and inserts them into the other. Warning: unlike
         *  std::copy it doesn't replace any values in the destination range.
         * 
         *  \param source       Input range from which values are copied.
         *  \param destination  Output range to which copied values are inserted.
         * 
         *  \return Resultant destination range.
         */
        template <class InputRange, class Size, class OutputRange>
        inline OutputRange& copy_n(InputRange source, Size n, OutputRange& destination)
        {
                for (size_t i = 0; i < n; source.pop_front(), ++i)
                {
                        destination.put(source.front());
                }
                return destination;
        }
}