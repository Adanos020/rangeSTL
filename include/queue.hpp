#pragma once


#include <algorithm>
#include <cstring>


namespace rs
{
        /** Queues are a type of container adaptor, specifically designed to operate in
         *  a FIFO context (first-in first-out), where elements are inserted into one
         *  end of the container and extracted from the other.
         */
        template <class Type>
        class queue
        {
        private: // Fields.

                Type* begin;
                Type* end;

        public: // Methods.

                queue() noexcept
                : begin(nullptr)
                , end(nullptr)
                {
                }

                queue(queue<Type>& other)
                : begin(new Type[other.size()])
                , end(begin + other.size())
                {
                        memcpy(begin, other.begin, other.size());
                }

                queue(queue<Type>&& other) noexcept
                : begin(other.begin)
                , end(other.end)
                {
                        other.begin = nullptr;
                        other.end = nullptr;
                }

                ~queue()
                {
                        if (!empty()) delete[] begin;
                }

                size_t size() const noexcept
                {
                        return end - begin;
                }

                void push(Type& value)
                {
                        put(value);
                }

                void push(Type&& value)
                {
                        put(value);
                }

                template <class... Args>
                void emplace(Args&... args)
                {
                        put(Type(args...));
                }

                void pop()
                {
                        pop_front();
                }

                void swap(queue<Type>& other) noexcept
                {
                        std::swap(this->begin, other.begin);
                        std::swap(this->end, other.end);
                }

        public: // Range methods.

                bool empty() const
                {
                        return !begin || !end || begin >= end;
                }

                queue<Type> save()
                {
                        return queue<Type>(*this);
                }

                void pop_front()
                {
                        assert(!empty());
                        *begin++ = 0;
                }

                Type& front()
                {
                        assert(!empty());
                        return *begin;
                }

                void put(Type& value)
                {
                        if (empty())
                        {
                                begin = new Type[1];
                                end = begin;
                        }
                        else
                        {
                                const size_t curr_size = size();
                                const Type* temp = begin;

                                begin = new Type[curr_size + 1];
                                memcpy(begin, temp, sizeof(Type) * curr_size);
                                delete[] temp;
                                
                                end = begin + curr_size;
                        }
                        *end++ = value;
                }

                void put(Type&& value)
                {
                        put(value); // This is now passing an lvalue reference so the method above is called.
                }
        };
}