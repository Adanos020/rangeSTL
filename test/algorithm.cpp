#include <algorithm.hpp>
#include <range.hpp>

#include <cstdio>


void test_find()
{
        int arr[] = {4, 3, 5, 6, 5, 7};
        rs::bicontig_range<int> cont(arr, arr + 6);

        auto five = rs::find(cont, 5);
        puts("Find test");
        rs::for_each(five, [](int i)
        {
                printf("%d ", i);
        });
        putchar('\n');
}

void test_find_end()
{
        int arr[] = {4, 3, 5, 6, 5, 7};
        rs::bicontig_range<int> cont(arr, arr + 6);

        auto end_five = rs::find_end(cont, 5);
        puts("Find end test");
        rs::for_each(end_five, [](int i)
        {
                printf("%d ", i);
        });
        putchar('\n');
}

int main()
{
        test_find();
        test_find_end();

        return 0;
}