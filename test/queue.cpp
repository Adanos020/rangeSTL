#include <algorithm.hpp>
#include <queue.hpp>

#include <cstdio>


void test_queue()
{
        rs::queue<int> qu;
        for (int i = 1; i <= 50; ++i)
        {
                qu.push(i);
        }

        printf("queue size: %zu\n", qu.size());
        rs::for_each(qu, [](int i)
        {
                printf("%d ", i);
        });
        putchar('\n');
}


// Run all.
int main()
{
        test_queue();

        return 0;
}