#include <algorithm.hpp>
#include <stack.hpp>

#include <cstdio>


void test_stack()
{
        rs::stack<int> st;
        for (int i = 1; i <= 50; ++i)
        {
                st.push(i);
        }

        printf("stack size: %zu\n", st.size());
        rs::for_each(st, [](int i)
        {
                printf("%d ", i);
        });
        putchar('\n');
}


int main()
{
        test_stack();

        return 0;
}