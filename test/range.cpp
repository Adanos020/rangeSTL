#include <algorithm.hpp>
#include <range.hpp>

#include <cstdio>


void test_retro()
{
        int arr[] = {4, 3, 5, 6, 5, 7};
        rs::bicontig_range<int> cont(arr, arr + 6);

        auto ret = rs::retro(cont);
        puts("Retro test");
        rs::for_each(ret, [](int i)
        {
                printf("%d ", i);
        });
        putchar('\n');
}

void test_infinite()
{
        class fibonacci_range
        {
        private:

                int current;
                int next;

        public:

                fibonacci_range()
                : current(0)
                , next(1)
                {
                }

                fibonacci_range(fibonacci_range& other)
                : current(other.current)
                , next(other.next)
                {
                }

                bool empty() const
                {
                        return false;
                }

                int front() const
                {
                        return current;
                }

                void pop_front()
                {
                        const int next_next = current + next;
                        current = next;
                        next = next_next;
                }

                fibonacci_range save()
                {
                        return fibonacci_range(*this);
                }
        };

        fibonacci_range fib;
        puts("Fibonacci range");
        for (int i = 0; i < 45; ++i, fib.pop_front())
        {
                printf("%d ", fib.front());
        }
        putchar('\n');
}

// Run all.
int main()
{
        test_retro();
        test_infinite();

        return 0;
}